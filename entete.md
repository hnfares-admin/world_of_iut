---
title:  "Projet de jeu d'aventure"
author: Laurent Pierron (Laurent.Pierron@inria.fr)
affiliation: INRIA / Université de Lorraine
tags: [nothing, nothingness]
documentclass: extarticle
date: 08 Février 2022
papersize: a4paper
fontsize: 9pt
geometry: "left=20mm, top=15mm, bottom=10mm, right=20mm, footskip=2mm"
...
